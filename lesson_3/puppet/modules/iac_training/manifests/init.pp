class iac_training {

  package { 'httpd':
    ensure => 'installed',
  }

  package { 'MariaDB-server':
    ensure => 'installed',
  }

  service { 'httpd':
    ensure => 'running',
    subscribe => Package['httpd']
  }

  service { 'mariadb':
    ensure => 'running',
    subscribe => Package['MariaDB-server']
  }

}
