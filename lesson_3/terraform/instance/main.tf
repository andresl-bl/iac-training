variable "ami" {}
variable "ec2-name" {}

provider "aws" {
  region = "sa-east-1"
  profile = "default"
}

data "template_file" "user_data" {
  vars {
    message = "IaC Training - Lesson 3"
  }
  template = "${file("files/user-data.sh")}"
}

resource "aws_instance" "training-node-01" {
  ami           = "${var.ami}"
  instance_type = "t2.nano"
  root_block_device {
    volume_size = "10"
    volume_type = "gp2"
    delete_on_termination = true
  }
  tags {
    Name = "${var.ec2-name}"
  }
  user_data = "${data.template_file.user_data.rendered}"
}

output "ec2-instance-public-ip" {
  value = "${aws_instance.training-node-01.public_ip}"
}
