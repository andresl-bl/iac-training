#!/bin/bash

PUPPET_DIR="/etc/puppet"
HOSTNAME="$(hostname)"
REPO_DIR="/tmp/iac-training"

echo ${message} > /tmp/data.log
sudo yum -y install git
rm -rfv $REPO_DIR
cd /tmp
sudo git clone https://andresl-bl@bitbucket.org/andresl-bl/iac-training.git
sudo cp -rfv $REPO_DIR/lesson_3/puppet/modules $PUPPET_DIR
sudo cp -rfv $REPO_DIR/lesson_3/files/*.repo /etc/yum.repos.d/
sudo yum repolist
rm -rfv $REPO_DIR
echo 'Puppet Code Downloaded\n'
cat <<EOF > $PUPPET_DIR/init.pp
node "$HOSTNAME" {
 include iac_training
}
EOF
sudo puppet apply $PUPPET_DIR/init.pp
