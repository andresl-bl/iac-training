provider "aws" {
  region = "sa-east-1"
  profile = "default"
}


resource "aws_vpc" "training-main-vpc" {
  cidr_block  = "10.188.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
 
  tags {
    Name = "training-dev-ca-main-vpc"
  }
}
 
resource "aws_subnet" "training-public" {
  vpc_id            = "${aws_vpc.training-main-vpc.id}"
  cidr_block        = "10.188.9.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "sa-east-1a"
 
  tags {
    Name = "training-public-subnet"
  }
}

resource "aws_security_group" "training-firewall-public" {
  name          = "training-firewall-public"
  description   = "training-firewall-public"
  vpc_id        = "${aws_vpc.training-main-vpc.id}"
 
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 
  tags {
    Name = "training-firewall-public"
  }
}

output "training-security-group-firewall-public" {
  value = "${aws_security_group.training-firewall-public.id}"
}

output "training-public-subnet" {
  value = "${aws_subnet.training-public.id}"
}

output "training-main-vpc" {
  value = "${aws_vpc.training-main-vpc.id}"
}
